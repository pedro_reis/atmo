import Vue from 'vue'
import VueRouter from 'vue-router'
import Page from '../views/Page.vue'
import VueGtag from 'vue-gtag'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Page,
    props: { slug: 'atmo' }
  },
  {
    path: '/:slug',
    name: 'page-show',
    component: Page,
    props: true
  }
]

const router = new VueRouter({
  mode: 'history',
  routes
})

Vue.use(VueGtag, { config: { id: 'UA-165929968-1' } }, router)

export default router
