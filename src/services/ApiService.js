import axios from 'axios'

const apiClient = axios.create({
  baseURL: process.env.VUE_APP_API_URL,
  withCredentials: false,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  },
  timeout: 10000
})

const fields = '_fields=id,title,slug,content,gallery'

export default {
  getPages() {
    return apiClient.get('/pages/?' + fields)
  },
  getPage(slug) {
    return apiClient.get('/pages/?slug=' + slug + '&' + fields)
  }
}
