import Vue from 'vue'
import Vuex from 'vuex'
import ApiService from '@/services/ApiService.js'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    loading: false,
    pages: [],
    page: {}
  },
  mutations: {
    SET_PAGES(state, pages) {
      state.pages = pages
    },
    SET_PAGE(state, page) {
      state.page = page
      state.loading = false
    }
  },
  actions: {
    fetchPages({ commit }) {
      return ApiService.getPages()
        .then(response => {
          commit('SET_PAGES', response.data)
          return response.data
        })
        .catch(error => {
          console.log('There was an error:', error.response)
        })
    },
    fetchPage({ commit, getters, state }, slug) {
      if (slug == state.page.slug) {
        return state.page
      }

      const page = getters.getPageBySlug(slug)

      if (page) {
        commit('SET_PAGE', page)
        return page
      } else {
        return ApiService.getPage(slug)
          .then(response => {
            commit('SET_PAGE', response.data[0])
            return response.data[0]
          })
          .catch(error => {
            console.log('There was an error:', error.response)
          })
      }
    }
  },
  getters: {
    getPageBySlug: state => slug => {
      return state.pages.find(page => page.slug === slug)
    }
  }
})
